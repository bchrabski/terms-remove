/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

var moduleRef;

$(function () {
  if (!window.RM) {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
  }
  gadgets.window.adjustHeight();
});
// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (!window.RM) {
    return;
  }
  if (selected.length === 1) {
    $("#removeTermsFromSelected").removeAttr("disabled");
    $("#removeTermsFromSelected").unbind();
    $(".status").removeClass("warning correct incorrect");

    $("#removeTermsFromSelected")
      .css("display", "inline")
      .on("click", function () {
        $("#removeTermsFromSelected").attr("disabled", "disabled");
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Removing terms in progress.");

        var error = false;

        RM.Data.getAttributes(selected[0], [RM.Data.Attributes.PRIMARY_TEXT], function (opResult) {
          if (opResult.code != RM.OperationResult.OPERATION_OK) {
            $("#removeTermsFromSelected").removeAttr("disabled");
            $(".status").removeClass("warning correct incorrect");
            $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
            error = true;
          } else {
            var toSave = [];
            opResult.data.forEach(function (artAttrs /* ArtifactAttributes */) {
              // Reset some attribute values
              artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT] = artAttrs.values[
                RM.Data.Attributes.PRIMARY_TEXT
              ].replace(/<\/?a(|\s+[^>]+)>/g, "");
              toSave.push(artAttrs);
            });
            RM.Data.setAttributes(toSave, function (respose) {
              if (respose.code != RM.OperationResult.OPERATION_OK) {
                $("#removeTermsFromSelected").removeAttr("disabled");
                $(".status").removeClass("warning correct incorrect");
                $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
                error = true;
              }
            });
          }
        });

        RM.Data.getLinkedArtifacts(selected[0], [RM.Data.LinkTypes.REFERENCES_TERM], function (response) {
          if (response.code != RM.OperationResult.OPERATION_OK) {
            $("#removeTermsFromSelected").removeAttr("disabled");
            $(".status").removeClass("warning correct incorrect");
            $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
            error = true;
          } else {
            var artLinks = response.data.artifactLinks;
            artLinks.forEach(function (linkDefinition) {
              linkDefinition.targets.forEach(function (target) {
                RM.Data.deleteLink(linkDefinition.art, RM.Data.LinkTypes.REFERENCES_TERM, target, function (response) {
                  if (response.code != RM.OperationResult.OPERATION_OK) {
                    $("#removeTermsFromSelected").removeAttr("disabled");
                    $(".status").removeClass("warning correct incorrect");
                    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
                    error = true;
                  }
                });
              });
            });
            if (!error) {
              $(".status").removeClass("warning correct incorrect");
              $("#removeTermsFromSelected").removeAttr("disabled");
              $(".status").addClass("correct").html("<b>Success:</b> All terms have been removed");
            }
          }
        });
      });
    $(".status").removeClass("warning correct incorrect");
    $(".status").removeClass("warning correct incorrect");
    $(".status").html("<b>Message:</b> One artifact is selected.");
  } else if (selected.length > 1) {
    $("#removeTermsFromSelected").removeAttr("disabled");
    $("#removeTermsFromSelected").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#removeTermsFromSelected")
      .css("display", "inline")
      .on("click", function () {
        $("#removeTermsFromSelected").attr("disabled", "disabled");
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Removing terms in progress.");

        var error = false;

        selected.forEach(function (item) {
          RM.Data.getAttributes(item, [RM.Data.Attributes.PRIMARY_TEXT], function (opResult) {
            if (opResult.code !== RM.OperationResult.OPERATION_OK) {
              $("#removeTermsFromSelected").removeAttr("disabled");
              $(".status").removeClass("warning correct incorrect");
              $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
              error = true;
            } else {
              var toSave = [];
              opResult.data.forEach(function (artAttrs /* ArtifactAttributes */) {
                // Reset some attribute values
                artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT] = artAttrs.values[
                  RM.Data.Attributes.PRIMARY_TEXT
                ].replace(/<\/?a(|\s+[^>]+)>/g, "");
                toSave.push(artAttrs);
              });
              RM.Data.setAttributes(toSave, function (item2) {});
            }
          });

          RM.Data.getLinkedArtifacts(item, [RM.Data.LinkTypes.REFERENCES_TERM], function (response) {
            if (response.code != RM.OperationResult.OPERATION_OK) {
              $("#removeTermsFromSelected").removeAttr("disabled");
              $(".status").removeClass("warning correct incorrect");
              $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
              error = true;
            } else {
              var artLinks = response.data.artifactLinks;
              artLinks.forEach(function (linkDefinition) {
                linkDefinition.targets.forEach(function (target) {
                  RM.Data.deleteLink(
                    linkDefinition.art,
                    RM.Data.LinkTypes.REFERENCES_TERM,
                    target,
                    function (response2) {
                      if (response2.code !== RM.OperationResult.OPERATION_OK) {
                        $("#removeTermsFromSelected").removeAttr("disabled");
                        $(".status").removeClass("warning correct incorrect");
                        $(".status")
                          .addClass("incorrect")
                          .html("<b>Error:</b> Something went wrong. Please try again.");
                        error = true;
                      }
                    }
                  );
                });
              });
            }

            if (!error) {
              $(".status").removeClass("warning correct incorrect");
              $("#removeTermsFromSelected").removeAttr("disabled");
              $(".status").addClass("correct").html("<b>Success:</b> All terms have been removed");
            }
          });
        });
      });
    $(".status").html(`<b>Message:</b> ${selected.length} artifacts are selected.`);
  } else {
    // clear the display area...
    $("#removeTermsFromSelected").attr("disabled", "disabled");
    $("#removeTermsFromSelected").unbind();
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html("<b>Message:</b> Select one or multiple objects.");
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  $("#removeTermsFromAllArtifactsInModule").attr("disabled", "disabled");
  moduleRef = null;
});

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      $("#removeTermsFromAllArtifactsInModule").removeAttr("disabled").on("click", removeAllTermsFromModule);
    } else {
      $("#removeTermsFromAllArtifactsInModule").attr("disabled", "disabled");
    }
  }
}

function removeAllTermsFromModule() {
  if (confirm("Do you want to continue?")) {
    $("#removeTermsFromSelected").attr("disabled", "disabled");
    $("#removeTermsFromAllArtifactsInModule").attr("disabled", "disabled");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("warning").html("<b>Message:</b> Removing terms in progress.");

    var error = false;

    RM.Data.getContentsAttributes(moduleRef, [RM.Data.Attributes.PRIMARY_TEXT], function (result) {
      if (result.code === RM.OperationResult.OPERATION_OK) {
        if (result.code != RM.OperationResult.OPERATION_OK) {
          $("#removeTermsFromSelected").removeAttr("disabled");
          $(".status").removeClass("warning correct incorrect");
          $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
          error = true;
        } else {
          var attrsToSet = [];
          result.data.forEach(function (item) {
            //
            RM.Data.getLinkedArtifacts(item.ref, [RM.Data.LinkTypes.REFERENCES_TERM], function (response) {
              if (response.code != RM.OperationResult.OPERATION_OK) {
                $("#removeTermsFromSelected").removeAttr("disabled");
                $(".status").removeClass("warning correct incorrect");
                $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
                error = true;
              } else {
                var artLinks = response.data.artifactLinks;
                artLinks.forEach(function (linkDefinition) {
                  linkDefinition.targets.forEach(function (target) {
                    RM.Data.deleteLink(
                      linkDefinition.art,
                      RM.Data.LinkTypes.REFERENCES_TERM,
                      target,
                      function (response) {
                        if (response.code != RM.OperationResult.OPERATION_OK) {
                          $("#removeTermsFromSelected").removeAttr("disabled");
                          $(".status").removeClass("warning correct incorrect");
                          $(".status")
                            .addClass("incorrect")
                            .html("<b>Error:</b> Something went wrong. Please try again.");
                          error = true;
                        }
                      }
                    );
                  });
                });
              }
            });

            if (item.values[RM.Data.Attributes.PRIMARY_TEXT].indexOf("</a>") > -1) {
              item.values[RM.Data.Attributes.PRIMARY_TEXT] = item.values[RM.Data.Attributes.PRIMARY_TEXT].replace(
                /<\/?a(|\s+[^>]+)>/g,
                ""
              );
              attrsToSet.push(item);
            }
          });
          RM.Data.setAttributes(attrsToSet, function (result) {
            if (result.code != RM.OperationResult.OPERATION_OK) {
              $("#removeTermsFromSelected").removeAttr("disabled");
              $(".status").removeClass("warning correct incorrect");
              $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
              error = true;
            } else {
              if (!error) {
                $(".status").addClass("correct").html("<b>Success:</b> All terms have been removed");
                $("#removeTermsFromSelected").removeAttr("disabled");
                $("#removeTermsFromAllArtifactsInModule").removeAttr("disabled");
              }
            }
          });

          if (attrsToSet.length == 0) {
            if (!error) {
              $(".status").addClass("correct").html("<b>Success:</b> All terms have been removed");
              $("#removeTermsFromSelected").removeAttr("disabled");
              $("#removeTermsFromAllArtifactsInModule").removeAttr("disabled");
            }
          }
        }
      }
    });
  }
}

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning").html("");

  $(".selected, .error").removeClass("selected error");
  $(".setRPNButton").css("display", "none");
}

function lockAllButtons() {
  $("#removeTermsFromAllArtifactsInModule").attr("disabled", "disabled");
  $("#removeTermsFromAllArtifactsInModule").removeClass("btn-primary");
  $("#removeTermsFromAllArtifactsInModule").addClass("btn-secondary");
  $("#removeTermsFromSelected").attr("disabled", "disabled");
  $("#removeTermsFromSelected").removeClass("btn-primary");
  $("#removeTermsFromSelected").addClass("btn-secondary");
}
