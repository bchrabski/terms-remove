### Terms Remove

The purpose of this widget is to remove links between the definitions (glossary terms) used and the artifact.


**Version:**

Version 1 (release date 15.2.2021)

**Description:**

Widget was created to remove unwanted references to definitions, terms, synonyms. The widget removes both references from the artifact text and links to other artifacts.


**Use:**

The widget works in two modes: for user selected artifacts and for entire modules. When running for whole modules, it is recommended to run the widget in change set in order not to delete links by running the widget incorrectly.



**Simple Mode:**

In this mode, select the artifacts which you want to remove links and hyperlinks to definitions and click the "Remove terms from all selected artifacts" button.


**Module Mode:**

In this mode, the widget goes through all artifacts in the module and removes hyperlinks from the text and links to definitions, terms, synonyms, and objects marked as glossary type.

